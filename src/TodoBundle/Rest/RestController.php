<?php
/**
 * Created by PhpStorm.
 * User: vince
 * Date: 10-Apr-17
 * Time: 20:52
 */

namespace TodoBundle\Rest;

use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\FOSRestController;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use TodoBundle\Entity\Todo;
use TodoBundle\Rest\Form\TodoCreateType;

class RestController extends FOSRestController
{
    /**
     * Post Route annotation.
     * @ApiDoc(
     *   description="Add new todo",
     *   section="todo",
     *   statusCodes={
     *       200="OK, todo registered",
     *       400="Validation error, wrong input",
     *       403="Invalid credentials"
     *   },
     *   input={
     *      "class"="TodoBundle\Rest\Form\TodoCreateType",
     *      "name"=""
     *   },
     * )
     */
    public function postTodoAction(Request $request)
    {
        $tokenService = $this->container->get('user_bundle_token');

        //(1)Check if valid user from event
        $user = $tokenService->getUser();
        if (!$user) {
            return new JsonResponse(array('error' => 'Apitoken niet gevonden'), 400);
        }

        //(2)Get form data and insert in form
        $todo = new Todo();
        $form = $this->createForm(new TodoCreateType(), $todo,
            array('validation_groups' => array('todo_api_create')));
        $form->submit($request->request->all());

        //(3)Check if valid
        if (!$form->isValid()) {
            return $this->view($form, 400);
        }

        //(4)Save users data
        $todo->setUser($user);
        $em = $this->getDoctrine()->getManager();
        $em->persist($todo);
        $em->flush();

        // (5)Send Id back
        return $this->view($todo->getId(), 200);
    }

    /**
     * Put Route annotation.
     * @Put("/todos/{id}")
     *
     * @ApiDoc(
     *   description="update todo",
     *   section="todo",
     *   statusCodes={
     *       200="OK, todo updated",
     *       400="Validation error, wrong input",
     *       403="Invalid credentials"
     *   },
     *   input={
     *      "class"="TodoBundle\Rest\Form\TodoCreateType",
     *      "name"=""
     *   },
     * )
     */
    public function putTodoAction(Request $request, Todo $todo)
    {
        $tokenService = $this->container->get('user_bundle_token');
        //(1)Check if valid user from event
        $user = $tokenService->getUser();
        if (!$user) {
            return new JsonResponse(array('error' => 'Apitoken niet gevonden'), 400);
        }

        // Check if todo exist at user
        $form = $this->createForm(new TodoCreateType(), $todo,
            array('validation_groups' => array('todo_api_update')));

        $form->submit($request->request->all());
        //(3)Check if valid
        if (!$form->isValid()) {
            return $this->view($form, 400);
        }

        // update the entity
        $em = $this->getDoctrine()->getManager();
        $em->flush();

        // (5)Send Id back
        return $this->view('', 200);
    }

    /**
     * GET Route annotation.
     * @ApiDoc(
     *   description="get todos",
     *   section="todo",
     *   statusCodes={
     *       200="OK",
     *       400="Validation error, wrong input",
     *       403="Invalid credentials"
     *   },
     *   output={
     *      "class"="TodoBundle\Rest\Output\Todo",
     *      "name"=""
     *   },
     * )
     */
    public function getTodosAction(Request $request)
    {
        $tokenService = $this->container->get('user_bundle_token');
        //(1)Check if valid user from event
        $user = $tokenService->getUser();
        if (!$user) {
            return new JsonResponse(array('error' => 'Apitoken niet gevonden'), 400);
        }

        $em = $this->getDoctrine()->getManager();
        $todos = $em->getRepository('TodoBundle:Todo')->getTodos($user);
        return $this->view($todos, 200);

    }


}