<?php

namespace TodoBundle\Rest\Output;

use JMS\Serializer\Annotation as JMS;

/**
 * Output of get todos
 */
class Todo
{
    /**
     * @JMS\Type("integer")
     */
    protected $id;

    /**
     * Title of the todo
     * @JMS\Type("string")
     * @JMS\SerializedName("title")
     */
    protected $title;

    /**
     * @JMS\Type("integer")
     * @JMS\SerializedName("priority")
     */
    protected $priority;

    /**
     * @JMS\Type("boolean")
     * @JMS\SerializedName("backlog")
     */
    protected $backlog;

    /**
     * @JMS\Type("boolean")
     * @JMS\SerializedName("completed")
     */
    protected $completed;

    /**
     * @JMS\Type("boolean")
     * @JMS\SerializedName("active")
     */
    protected $active;

    /**
     * @JMS\Type("string")
     * @JMS\SerializedName("completedDate")
     */
    protected $completedDate;

    /**
     * @JMS\Type("string")
     * @JMS\SerializedName("putInbacklogDate")
     */
    protected $putInbacklogDate;

    /**
     * @JMS\Type("string")
     * @JMS\SerializedName("created")
     */
    protected $created;

}

