<?php
/**
 * Created by PhpStorm.
 * User: vince
 * Date: 10-Apr-17
 * Time: 21:00
 */


namespace TodoBundle\Rest\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TodoCreateType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', 'text', array(
                'description' => 'title of the todo',
                'required' => true
            ))->add('priority', 'integer', array(
                'label' => 'priority',
                'description' => 'set a priority for the token',
                'required' => true
            ))->add('backlog', 'checkbox', array(
                'description' => 'put todo in backlog',
                'required' => true
            ))->add('completed', 'checkbox', array(
                'description' => 'put todo in completed',
                'required' => true
            ))->add('active', 'checkbox', array(
                'description' => 'put todo in active',
                'required' => true
            ))->add('completedDate', 'text', array(
                'description' => 'set date completed',
                'required' => true
            ))->add('putInbacklogDate', 'text', array(
                'description' => 'set date went to backlog',
                'required' => true
            ))->add('created', 'text', array(
                'description' => 'set date created',
                'required' => true
            ))->add('deleted', 'checkbox', array(
                'description' => 'set date deleted',
                'required' => true
            ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'TodoBundle\Entity\Todo',
            'allow_extra_fields' => true,
            'csrf_protection' => false,
        ));
    }

    public function getName()
    {
        return 'todo_bundle_create';
    }
}