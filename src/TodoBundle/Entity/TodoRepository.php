<?php

namespace TodoBundle\Entity;

use Doctrine\DBAL\LockMode;
use Doctrine\ORM\EntityRepository;
use UserBundle\Entity\User;

class TodoRepository extends EntityRepository
{

    /**
     * Get todos of user
     * @param $id integer
     * @return array
     */
    public function getTodos(User $user)
    {
        $query = $this->_em->createQuery("
            SELECT t
            FROM TodoBundle:Todo t
            WHERE t.deleted != 1
            AND t.user = :user
        ")->setParameter('user', $user);

        return $query->getResult();
    }

}