<?php
/**
 * Created by PhpStorm.
 * User: vince
 * Date: 10-Apr-17
 * Time: 20:14
 */

namespace TodoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use UserBundle\Entity\User;
use JMS\Serializer\Annotation as JMS;

/**
 * Class Todo
 * @ORM\Table(name="user_todo")
 * @ORM\Entity(repositoryClass="TodoBundle\Entity\TodoRepository")
 */
class Todo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="created", type="string", nullable=true)
     */
    private $created;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User", inversedBy="Todos")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @var integer
     * @ORM\Column(name="active", type="boolean", nullable=true)
     */
    private $active;

    /**
     * @var integer
     * @ORM\Column(name="backlog", type="boolean", nullable=true)
     */
    private $backlog;

    /**
     * @var string
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @var integer
     * @ORM\Column(name="priority", type="smallint",nullable=true, length=0, options={"default" : 0, "unsigned"=true})
     */
    private $priority;

    /**
     * @var integer
     * @ORM\Column(name="deleted", type="boolean",nullable=true, length=0, options={"default" : 0, "unsigned"=true})
     */
    private $deleted;

    /**
     * @var integer
     * @ORM\Column(name="completed", type="boolean",nullable=true, length=0, options={"default" : 0, "unsigned"=true})
     */
    private $completed;


    /**
     * @JMS\SerializedName("completedDate")
     * @var integer
     * @ORM\Column(name="completed_date", type="string", nullable=true)
     */
    private $completedDate;

    /**
     * @JMS\SerializedName("putInbacklogDate")
     * @var integer
     * @ORM\Column(name="backlog_date", type="string", nullable=true)
     */
    private $putInbacklogDate;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Todo
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set backlog
     *
     * @param integer $backlog
     * @return Todo
     */
    public function setBacklog($backlog)
    {
        $this->backlog = $backlog;

        return $this;
    }

    /**
     * Get backlog
     *
     * @return integer
     */
    public function getBacklog()
    {
        return $this->backlog;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Todo
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set priority
     *
     * @param integer $priority
     * @return Todo
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;

        return $this;
    }

    /**
     * Get priority
     *
     * @return integer
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * Set deleted
     *
     * @param integer $deleted
     * @return Todo
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return integer
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set user
     *
     * @param \UserBundle\Entity\User $user
     * @return Todo
     */
    public function setUser(\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set completed
     *
     * @param integer $completed
     * @return Todo
     */
    public function setCompleted($completed)
    {
        $this->completed = $completed;

        return $this;
    }

    /**
     * Get completed
     *
     * @return integer 
     */
    public function getCompleted()
    {
        return $this->completed;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Todo
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set completedDate
     *
     * @param string $completedDate
     * @return Todo
     */
    public function setCompletedDate($completedDate)
    {
        $this->completedDate = $completedDate;

        return $this;
    }

    /**
     * Get completedDate
     *
     * @return string 
     */
    public function getCompletedDate()
    {
        return $this->completedDate;
    }

    /**
     * Set backlogDate
     *
     * @param string $backlogDate
     * @return Todo
     */
    public function setBacklogDate($backlogDate)
    {
        $this->backlogDate = $backlogDate;

        return $this;
    }

    /**
     * Get backlogDate
     *
     * @return string 
     */
    public function getBacklogDate()
    {
        return $this->backlogDate;
    }

    /**
     * Set putInbacklogDate
     *
     * @param string $putInbacklogDate
     * @return Todo
     */
    public function setPutInbacklogDate($putInbacklogDate)
    {
        $this->putInbacklogDate = $putInbacklogDate;

        return $this;
    }

    /**
     * Get putInbacklogDate
     *
     * @return string 
     */
    public function getPutInbacklogDate()
    {
        return $this->putInbacklogDate;
    }
}
