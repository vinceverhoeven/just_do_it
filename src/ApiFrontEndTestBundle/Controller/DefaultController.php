<?php

namespace ApiFrontEndTestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('ApiFrontEndTestBundle:Default:index.html.twig');
    }
}
