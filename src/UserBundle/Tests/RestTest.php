<?php

namespace Bundle\UserBundle\Tests;

use Liip\FunctionalTestBundle\Test\WebTestCase;
use Doctrine\ORM\EntityManager;
use UserBundle\Entity\User;

class RestTest extends WebTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $entityManager;
    protected $register_url = "/api/user/register";

    const TEST_USER = "testUser@gmail.com";

    /**
     * @return null
     */
    public function setUp()
    {
        $this->entityManager = $this->getContainer()->get('doctrine')->getManager();
        parent::setUp();
    }

    protected function removeUser()
    {
        $entity = $this->entityManager->getRepository('UserBundle:User');
        $user = $entity->findOneBy(array('email' => self::TEST_USER));

        if ($user != null) {
            $this->entityManager->remove($user);
            $this->entityManager->flush();
        }
    }

    /**
     * Test user registration with valid email and password
     */
    public function testRegisterUserValid()
    {
        // remove user from db
        $this->removeUser();

        $request = array(
            'email' => self::TEST_USER,
            'plainPassword' => 'password',
            'username' => 'phpunit',
        );

        $client = static::createClient();
        $client->request('POST', $this->register_url, $request);
        $response = $client->getResponse();
        $this->assertTrue($response->getStatusCode() == 200);
    }

    /**
     * Test user registration with invalid registration
     */
    public function testRegisterUserDoubleEmail()
    {
        // notice we don't remove the user from previous test
        //$this->removeUser();
        $request = array(
            'email' => self::TEST_USER,
            'plainPassword' => 'password',
            'username' => 'phpunit',
        );

        $client = static::createClient();
        $client->request('POST', $this->register_url, $request);

        $response = $client->getResponse();
        $this->assertTrue($response->getStatusCode() == 400);

        // remove user from db
        $this->removeUser();
    }

    /**
     * Test user registration with invalid registration
     */
    public function testRegisterUserEmptyInput()
    {
        $request = array(
            'email' => '',
            'plainPassword' => '',
            'username' => '',
        );

        $client = static::createClient();
        $client->request('POST', $this->register_url, $request);

        $response = $client->getResponse();
        $this->assertTrue($response->getStatusCode() == 400);
        $data = json_decode($response->getContent(), true);
        $this->assertTrue(count($data["errors"]["errors"]) == 3);
    }

}
