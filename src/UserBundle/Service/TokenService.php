<?php
/**
 * Created by PhpStorm.
 * User: vince
 * Date: 09-Apr-17
 * Time: 16:35
 */

namespace UserBundle\Service;

use Doctrine\ORM\EntityManager;
use UserBundle\Entity\User;

class TokenService
{


    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var  string
     */
    protected $generatedToken;


    protected $user;

    /**
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function generateToken()
    {
        $this->generatedToken = base64_encode(hash_hmac('sha512', uniqid(null, true), uniqid(), true));
        return $this->generatedToken;
    }

    public function checkValidToken($token)
    {
        $tokenEntity = $this->em->getRepository('UserBundle:ApiToken');
        $apiToken = $tokenEntity->findOneBy(array('token' => $token));
        if ($apiToken) {
            return true;
        }
        return false;
    }

    public function getUserByToken($token)
    {

        $tokenEntity = $this->em->getRepository('UserBundle:ApiToken');
        $apiToken = $tokenEntity->findOneBy(array('token' => $token));

        if (!$apiToken) {
            return false;
        }

        $userEntity = $this->em->getRepository('UserBundle:User');
        $user = $userEntity->findOneBy(array('id' => $apiToken->getUser()->getId()));

        if (!$user) {
            return false;
        }

        return $user;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }


}
