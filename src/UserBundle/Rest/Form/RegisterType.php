<?php

namespace UserBundle\Rest\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class RegisterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', 'text', array(
                'description' => 'Unique username.',
                'required' => true,
                'error_bubbling' => true
            ))
            ->add('email', 'email', array(
                'label' => 'e-mail address',
                'description' => 'Unique e-mail address. Also used for authentication.',
                'error_bubbling' => true
            ))
            ->add('plainPassword', 'text', array(
                'description' => 'Password is used in desktop application and for API token generation.',
                'required' => true,
                'error_bubbling' => true
            ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'UserBundle\Entity\User',
            'intention' => 'registration',
            'csrf_protection' => false,
        ));
    }

    public function getName()
    {
        return 'user_bundle_register';
    }
}