<?php
/**
 * Created by PhpStorm.
 * User: vince
 * Date: 11-Apr-17
 * Time: 20:57
 */

namespace UserBundle\Rest\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TokenType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('token', 'text', array(
                'description' => 'Unique token.',
                'required' => true,
                'error_bubbling' => true
            ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'UserBundle\Entity\ApiToken',
            'csrf_protection' => false,
            'allow_extra_fields' => true
        ));
    }

    public function getName()
    {
        return 'user_bundle_token';
    }
}