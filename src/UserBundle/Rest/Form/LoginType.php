<?php
/**
 * Created by PhpStorm.
 * User: vince
 * Date: 08-Apr-17
 * Time: 19:02
 */

namespace UserBundle\Rest\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class LoginType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            ->add('email', 'email', array(
                'description' => 'your e-mail address',
                'required' => true,
            ))
            ->add('plainPassword', 'text', array(
                'description' => 'your password',
                'required' => true,
            ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'UserBundle\Entity\User',
            'csrf_protection' => false,
        ));
    }

    public function getName()
    {
        return 'tokenrequest';
    }

}