<?php

namespace UserBundle\Rest;

use FOS\RestBundle\Controller\FOSRestController;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;
use UserBundle\Rest\Form\RegisterType;
use UserBundle\Entity\User;
use UserBundle\Entity\ApiToken;
use UserBundle\Rest\Output\NewUser;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations\Route;
use UserBundle\Rest\Form\LoginType;
use Symfony\Component\HttpFoundation\JsonResponse;
use UserBundle\Rest\Output\TokenResponse;
use UserBundle\Service\TokenService;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class RestController extends FOSRestController
{
    /**
     * @route("user/register")
     * @ApiDoc(
     *   description="Register new user",
     *   views = { "default", "oauth" },
     *   section="user",
     *   statusCodes={
     *       200="OK, user registered",
     *       400="Validation error, wrong input"
     *   },
     *   input={
     *      "class"="UserBundle\Rest\Form\RegisterType",
     *      "name"=""
     *   },
     *   output="UserBundle\Rest\Output\NewUser"
     * )
     */
    public function postUserRegisterAction(Request $request)
    {
        // 1) build the form
        $user = new User();
        $form = $this->createForm(new RegisterType(), $user, array(
                'validation_groups' => array('restregistration'))
        );

        // 2) handle the submit
        $form->submit($request->request->all());

        //(3) Check if valid form data
        if (!$form->isValid()) {
            return $this->view($form, 400);
        }

        // (4) save the User!
        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();

        return $this->view('', 200);
    }

    /**
     * @ApiDoc(
     *   description="Get API token for user",
     *   section="user",
     *   statusCodes={
     *       200="OK",
     *       400="Validation error, wrong input",
     *       403="Invalid credentials"
     *   },
     *   input={
     *       "class"="UserBundle\Rest\Form\LoginType",
     *       "name"=""
     *   },
     *   output="UserBundle\Rest\Output\TokenResponse"
     * )
     */
    public function postUserTokenAction(Request $request)
    {
        $user = new User();
        $input = $request->request->all();
        $form = $this->createForm(new LoginType(), $user, array('validation_groups' => array('rest_login')));

        // 2) handle the submit
        $form->submit($request->request->all());

        //(3) Check if valid form data
        if (!$form->isValid()) {
            return $this->view($form, 400);
        }

        $user_manager = $this->get('fos_user.user_manager');
        $user = $user_manager->findUserByEmail($user->getEmail());
        if (!$user) {
            return new JsonResponse(array('error' => "We couldn't find that e-mail"), 403);
        }

        $factory = $this->get('security.encoder_factory');
        $encoder = $factory->getEncoder($user);

        if(!$encoder->isPasswordValid($user->getPassword(), $input['plainPassword'], $user->getSalt())){
            return new JsonResponse(array('error' => "Password is wrong"), 403);
        }

        // (5) save Apitoken
        $tokenService = $this->container->get('user_bundle_token');
        $tokenHash = $tokenService->generateToken();

        // TODO update if already exist
        $apiToken = new ApiToken();
        $apiToken->setCreated(new \DateTime());
        $apiToken->setUser($user);
        $apiToken->setToken($tokenHash);

        $em = $this->getDoctrine()->getManager();
        $em->persist($apiToken);
        $em->flush();

        return $this->view(array('token' => $apiToken->getToken()), 200);
    }
}

