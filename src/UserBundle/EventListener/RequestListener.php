<?php
/**
 * Created by PhpStorm.
 * User: vince
 * Date: 11-Apr-17
 * Time: 20:22
 */

namespace UserBundle\EventListener;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use UserBundle\Entity\ApiToken;
use UserBundle\Rest\Form\TokenType;

class RequestListener
{

    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        /* @var $request \Symfony\Component\HttpFoundation\Request */
        $request = $event->getRequest();
        //(1)check if json request
        if ($request->getRequestFormat() == 'json') {
            // check if security is needed
            $lastWord = substr($request->getUri(), strrpos($request->getUri(), '/') + 1);
            if (in_array($lastWord, array('register', 'tokens'))) {
                return;
            }

            //(2)get services
            $tokenService = $this->container->get('user_bundle_token');
            $formService = $this->container->get('form.factory');

            //(3)create token form
            $apiToken = new ApiToken();
            $form = $formService->create(new TokenType(), $apiToken, array('validation_groups' => array('user_bundle_token')));
            $token = array('token' => $request->headers->get('token'));
            $form->submit($token);

            //(4)check if valid
            if (!$form->isValid()) {
                echo json_encode(array("error" => $form->getErrors()->current()->getMessage()));
                die;
            }

            //(5)get and check for valid token
            $user = $tokenService->getUserByToken($token['token']);

            //(6) set user
            $tokenService->setUser($user);
        }
    }
}